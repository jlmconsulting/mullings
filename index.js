import { AppRegistry } from 'react-native';
import App from './src';
import "babel-polyfill"

console.disableYellowBox = true;
AppRegistry.registerComponent('mullings', () => App);
