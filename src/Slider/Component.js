/**
 * Created by jlmconsulting on 5/2/18.
 */
import React, {Component} from 'react';
import {
    Platform,
    WebView,
    View,
    ActivityIndicator,
    Dimensions,
    ImageBackground,
    TouchableOpacity,
    Alert
} from 'react-native';
import FlipCard from '../utils/flipCard'
import styles from './Styles'
import ProgressImage from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';

const HomeComponent = props => {
    return (
        <View style={styles.container}>
            <ProgressImage indicatorProps={{ color: '#2eb5ee'}} resizeMode="contain" source={props.slideImage}
                           indicator={ProgressBar} style={styles.ImageBackground}>
                {props.displayNote ?
                    <FlipCard
                        text={props.flipCardMessage}
                        backText=""
                        color="#9439FF"
                        backColor="#B9D4BB"
                        fontSize={20}
                    />
                    : null}
            </ProgressImage>
        </View>
    );
}

export default HomeComponent;