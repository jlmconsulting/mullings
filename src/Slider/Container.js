/**
 * Created by jlmconsulting on 5/2/18.
 */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    Text,
    Platform,
    StyleSheet,
    WebView,
    View,
    ActivityIndicator,
    Dimensions,
    ImageBackground,
    TouchableOpacity,
    Alert
} from 'react-native';
import styles from './Styles'
import SlideComponent from './Component';
import {Actions} from 'react-native-router-flux';

import {observer, inject, when} from 'mobx-react'

@inject('store')
@observer
class App extends Component {

    async _updateState(key, value) {
        await this.props.store.setCoreText(key, value);
    };
    render() {
        const { slideImage } = this.props;
        const { dataLoading, displayNote } = this.props.store
        return (
            <SlideComponent
                dataLoading={dataLoading}
                updateState={this._updateState.bind(this)}
                slideImage={slideImage}
                displayNote={displayNote}
                >
            </SlideComponent>
        );
    }
}
export default App
