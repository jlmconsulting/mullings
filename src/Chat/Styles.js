/**
 * Created by jlmconsulting on 1/26/18.
 */
import {
    StyleSheet,
    Dimensions
} from "react-native";

const deviceDimes = Dimensions.get('window');
const paddedWidth = deviceDimes.width - 30;

export default StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        marginTop:50
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5
    },
    bannerImage:{
        width:paddedWidth,
        height:paddedWidth/2.75,
        resizeMode:'contain',
        borderTopLeftRadius:5,
        borderTopRightRadius:5
    }
});