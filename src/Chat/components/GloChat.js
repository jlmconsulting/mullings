import React, {Component, PropTypes} from 'react';
import {Text, View, StyleSheet} from "react-native";
import {GiftedChat} from "react-native-gifted-chat";
import {observer, inject} from 'mobx-react'
import {Actions} from 'react-native-router-flux'


@inject('store')
@observer
class GloChat extends Component {
    constructor(props) {
        super(props);
        this.state = {name: false};
    }

    state = {
        messages: []
    };

    componentWillMount() {

        // this.getMessages()
    }

    async getMessages() {
        // await this.props.chatStore.getMessages();
        // this.setState(this.props.chatStore.messages);
    }

    async sendMessages(message) {
        // await this.props.chatStore.sendMessage(message);
    }

    render() {
        return (
            <View style={styles.container}>
                <GiftedChat
                    messages={this.state.messages}
                    onSend={message=>this.sendMessages(message)}
          //           user={{
          //   _id: this.props.chatStore.getUid(),
          //   name: "test name"
          // }}
                />
            </View>
        );
    }
    componentWillUnmount() {
       this.closeChat()
    }

    async closeChat(){
        // await  this.props.chatStore.closeChat()
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: "#DAFCD8"
    }
});
export default GloChat;