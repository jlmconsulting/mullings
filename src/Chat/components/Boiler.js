import React, {Component} from "react";
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Button,
    StatusBar,
    TextInput,
    TouchableOpacity,
    Dimensions,
    Image
} from "react-native";

import firebase from "react-native-firebase";
import {observer, inject} from 'mobx-react'
import {Actions} from 'react-native-router-flux'


@inject('store')
@observer
class Boiler extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            name: "Test Name Tester",
            password: "",
            password_confirmation: "",
            errorMessage: null,
            loading: false
        };
       //this.getFireToken()
    }

    getRef() {
        return firebase.database().ref();
    }

    componentWillMount() {

        // firebase
        //     .auth()
        //     .signInWithEmailAndPassword('jswan1972@yahoo.co.uk', 'Samsaung1972')
        //     .then(() => {
        //         this.setState({ loading: false });
        //     })
        //     .catch(error => {
        //         var errorCode = error.code;
        //         var errorMessage = error.message;
        //         this.setState({
        //             errorMessage,
        //             loading: false
        //         });
        //     });
        //
        // firebase.auth().onAuthStateChanged(user => {
        //     if (user) {
        //         this.setState({
        //             loading: false
        //         });
        //     }
        // });
        console.log('Chat Room Online!!!!!')
        // console.log('this state',this.state)
    }
    async getFireToken(){
        await this.props.chatStore.getFireToken();
                this.setState({
                    token: this.props.mainLogin.token
                });
        // console.log('this state',this.state)
    }
    handleChange() {
        Actions.GloChat()
    }

    handleFriends() {
        Actions.Friendlist()
    }

    handleLogOut() {

        //await this.props.maninLogin.logOut()
        Actions.MainScreen();
    }

    render() {
        return (
            <View style={styles.containerl}>
                <StatusBar barStyle="light-content" backgroundColor="#00796B"/>
                <Text/>
                <Text/>
                    <Button
                        primary
                        title="Enter chat"
                        //style={styles.rightButton}
                        onPress={this.handleChange.bind(this)}
                    >
                        Enter chat
                    </Button>
                <Text/>

                <Text/>
                <Text/>
                <Button
                    primary
                    title="Log out"
                    //style={styles.rightButton}
                    onPress={this.handleLogOut.bind(this)}
                >
                    Log out
                </Button>
            </View>
        );
    }
}
//
// <Text/>
//
// <Button
// primary
// title="Friend List"
// //style={styles.rightButton}
// onPress={this.handleFriends.bind(this)}
// >
// Friend List
// </Button>
export default Boiler;
var styles = StyleSheet.create({
    title: {
        marginLeft: 20,
        marginTop: 10,
        fontSize: 20
    },
    nameInput: {
        height: 40,
        borderWidth: 1,
        borderColor: "black",
        margin: 20
    },
    buttonStyle: {
        marginLeft: 20,
        margin: 20
    },
    containerl: {
        flex: 1
    }
});

