/**
 * Created by jlmconsulting on 3/24/18.
 */
import React, {Component} from 'react';
import {Text, View, Button, Alert, Image, TouchableOpacity, ImageBackground} from 'react-native';
import MapView from 'react-native-maps';
import Spinner from 'react-native-loading-spinner-overlay';
import styles from './StyleSheet'

import Icon from 'react-native-vector-icons/FontAwesome';

const KFDirections = props =>{

    console.log(props);

    return (

        <View style={styles.container}>
            <MapView style={styles.map} initialRegion={{
               latitude:14.6849,
               longitude:121.1054,
               latitudeDelta: 1,
               longitudeDelta: 1
            }}>

                {!!props.state.latitude && !!props.state.longitude && <MapView.Marker
                    coordinate={{"latitude":props.state.latitude,"longitude":props.state.longitude}}
                    title={"Your Location"}
                />}

                {!!props.state.cordLatitude && !!props.state.cordLongitude &&
                    <MapView.Marker.Animated
                        coordinate={{"latitude":props.state.cordLatitude,"longitude":props.state.cordLongitude}}
                        title={"Your Destination"}

                    >
                        <MapView.Callout onPress={()=>props.handleMarkerPress()}>
                            <View style={styles.deliveryCardTop}>
                                <View style={{ flex: 1,justifyContent: 'center', padding: 10}}>

                                    <View style={styles.riderDetails}>
                                        <View style={styles.trackerRow}>

                                            <Text style={styles.trackerTime}>Your destination is 10 mins away!</Text>
                                        </View>
                                    </View>
                                    <TouchableOpacity style={styles.messageButton} onPress={()=>props.handleChatPress()}>
                                        <Text style={styles.messageText}>
                                            <Icon name='envelope' size={12}/>   Message
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </MapView.Callout>
                    </MapView.Marker.Animated>}

                {!!props.state.latitude && !!props.state.longitude && props.state.x == 'true' && <MapView.Polyline
                    coordinates={props.state.coords}
                    strokeWidth={5}
                    strokeColor="#5f9f0d"/>
                }

                {!!props.state.latitude && !!props.state.longitude && props.state.x == 'error' && <MapView.Polyline
                    coordinates={[
                          {latitude: props.state.latitude, longitude: props.state.longitude},
                          {latitude: props.state.cordLatitude, longitude: props.state.cordLongitude}
                      ]}
                    strokeWidth={5}
                    strokeColor="#5f9f0d"/>
                }
            </MapView>

        </View>

    );
};

export default KFDirections
