/**
 * Created by jlmconsulting on 5/2/18.
 */
import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import { Provider } from 'mobx-react';
import stores from './store';
import Routers from './router';
import codePush from 'react-native-code-push'
import SplashScreen from 'react-native-splash-screen';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        global.self = global;
    }

    componentDidMount() {

        SplashScreen.hide();

    }
    render() {
        return (
            <Provider {...stores}>
                <Routers />
            </Provider>
        );
    }
}

const codePushOptions = {
    installMode: codePush.InstallMode.ON_NEXT_RESTART,
    checkFrequency: codePush.CheckFrequency.ON_APP_RESUME
};

// export default App = codePush(codePushOptions)(App)
export default App = codePush()(App)