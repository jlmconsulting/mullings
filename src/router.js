/**
 * Created by jlmconsulting on 5/2/18.
 */
import React from 'react';
import {Drawer, Reducer, Router, Scene, Stack, Actions, Lightbox} from 'react-native-router-flux';
import Chat from './Chat';
import Directions from './Directions';
import Gallery from './Gallery';
import ImageUpload from './ImageUpload';
import Map from './Map';
import Profile from './Profile';
import VideoUpload from './VideoUpload';
import WebView from './WebView';



import Home from './Home';
import Main from './Main';
import ImageLightbox from './utils/Lightbox';
import Slider from './Slider';
// import { size } from './utils/coreUtil';
// import { BLUE } from './themes/core';

const reducerCreate = params => {
    const defaultReducer = new Reducer(params);
    return (state, action) => {
        // console.log('ACTION:', action);
        return defaultReducer(state, action);
    };
};

const getSceneStyle = () => ({
    backgroundColor: '#7FBFFF',
});

// <Scene hideNavBar key="home" component={Home} back/>
const Routers = () => (
    <Router createReducer={reducerCreate} getSceneStyle={getSceneStyle}>
        <Lightbox>
            <Stack hideNavBar key="root" titleStyle={{ alignSelf: 'center' }}>
                <Scene hideNavBar key="home" component={Home} back/>
                <Scene hideNavBar key="Main" component={Main} back/>
                <Stack
                    back
                    backTitle="Back"
                    key="WebView"
                    duration={1}
                    navTransparent
                ><Scene  key="WebView" component={WebView} back/>
                </Stack>
                <Stack
                    back
                    backTitle="Back"
                    key="Slider"
                    duration={1}
                    navTransparent
                ><Scene  key="Slider" component={Slider} back/>
                </Stack>
                <Stack
                    back
                    backTitle="Back"
                    key="Chat"
                    duration={1}
                    navTransparent
                ><Scene  key="Chat" component={Chat} back/>
                </Stack>
                <Stack
                    back
                    backTitle="Back"
                    key="Directions"
                    duration={1}
                    navTransparent
                ><Scene  key="Directions" component={Directions} back/>
                </Stack>
                <Stack
                    back
                    backTitle="Back"
                    key="Gallery"
                    duration={1}
                    navTransparent
                ><Scene  key="Gallery" component={Gallery} back/>
                </Stack>
                <Stack
                    back
                    backTitle="Back"
                    key="ImageUpload"
                    duration={1}
                    navTransparent
                ><Scene  key="ImageUpload" component={ImageUpload} back/>
                </Stack>
                <Stack
                    back
                    backTitle="Back"
                    key="Map"
                    duration={1}
                    navTransparent
                ><Scene  key="Map" component={Map} back/>
                </Stack>
                <Stack
                    back
                    backTitle="Back"
                    key="Profile"
                    duration={1}
                    navTransparent
                ><Scene  key="Profile" component={Profile} back/>
                </Stack>
                <Stack
                    back
                    backTitle="Back"
                    key="VideoUpload"
                    duration={1}
                    navTransparent
                ><Scene  key="VideoUpload" component={VideoUpload} back/>
                </Stack>
            </Stack>
            <Scene key="lightbox" component={ImageLightbox}/>
        </Lightbox>
    </Router>
);

export default Routers;