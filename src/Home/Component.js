/**
 * Created by jlmconsulting on 5/2/18.
 */
import React, {Component} from 'react';
import {
    Platform,
    WebView,
    View,
    ActivityIndicator,
    Dimensions,
    ImageBackground,
    TouchableOpacity,
    Alert
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import styles from './Styles'

const HomeComponent = props =>{

    let image = props.session? require('../Images/bluegif.gif') : require('../Images/bluegif.gif')
    return (
        <View style={styles.container}>

            <ImageBackground source={image} style={styles.ImageBackground} blurRadius={1}
                             onLoad={props.ActivityIndicatorLoadingView}>

                { (!props.session)
                    ?
                    <TouchableOpacity onPress={props.facebookLogin}>
                        <View style={styles.navIconContainer}>
                            <FontAwesome name="power-off" size={60} style={styles.navIconOff}/>
                        </View>
                    </TouchableOpacity>
                    :
                    <TouchableOpacity onPress={props.gotoMain}>
                        <View style={styles.navIconContainer}>
                            <FontAwesome name="power-off" size={60} style={styles.navIconOn}/>
                        </View>
                    </TouchableOpacity>
                }
            </ImageBackground>

        </View>
    );
}

export default HomeComponent;