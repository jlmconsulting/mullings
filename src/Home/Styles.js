/**
 * Created by jlmconsulting on 5/2/18.
 */
/**
 * Created by jlmconsulting on 1/26/18.
 */
import {
    StyleSheet,
    Dimensions,
    Platform
} from 'react-native';

const deviceDimes = Dimensions.get('window');
const paddedWidth = deviceDimes.width-60;

export default StyleSheet.create({
    container: {
        flex: 1,
    },
    ImageBackground: {
        flex: 1,
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
      
        // width: Dimensions.get('window').width,
        // height: Dimensions.get('window').height

    },
    navItem: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    navIconContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    navIconOff: {
        color: 'red',
        alignItems: 'center',
        justifyContent: 'center',
    },
    navIconOn: {
        color: 'green',
        alignItems: 'center',
        justifyContent: 'center',
    },
    loanIcon: {
        height: 100,
        resizeMode: 'contain'
    },
    navTextContainer: {
        justifyContent: 'center',
        marginRight: 10,
        flex: 3
    },

    ActivityIndicatorStyle: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'

    }
});