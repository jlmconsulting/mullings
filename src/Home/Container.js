/**
 * Created by jlmconsulting on 5/2/18.
 */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    Text,
    Platform,
    StyleSheet,
    WebView,
    View,
    ActivityIndicator,
    Dimensions,
    ImageBackground,
    TouchableOpacity,
    Alert
} from 'react-native';
import {AccessToken, LoginManager, GraphRequestManager, GraphRequest} from 'react-native-fbsdk';
import { Actions } from 'react-native-router-flux';
import firebase from 'react-native-firebase'
import styles from './Styles'

import HomeComponent from './Component';


import {observer, inject} from 'mobx-react'

@inject('store')
@observer
class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userDetails: {},
            session: {
                data: {},
                user: false,
                faceBook: {}

            }
        };
        global.self = global;
    }

    componentWillMount() {
        this.setState({
            ...this.props.store.getStoreState
        })
        if (!this.props.store.PhonePermission)
            this.getLocationPermission();
        else
            this.props.store.getCurrentLocation()


    }

    async getLocationPermission() {
        await this.props.store.getPermission('location')
        await this.props.store.getPermission('notification')
        console.log('this.props.store.permissions', this.props.store.PhonePermission)
    }

    ActivityIndicatorLoadingView() {

        return (

            <ActivityIndicator
                color='#009688'
                size='large'
                style={styles.ActivityIndicatorStyle}
            />
        );
    }

    goToLogOff() {


        Alert.alert('Did press')
    }

    responseInfoCallback = (error, result) => {
        if (error) {
            console.log('Error fetching data: ' + error.toString());
        } else {
            this.setUserDetails(result)
            if (result.name && result.email)
                firebase
                    .auth()
                    .createUserAndRetrieveDataWithEmailAndPassword(result.email, result.id)
                    .then(() => {
                        this.setState({loading: false});

                    })
                    .catch(error => {
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        this.setState({errorMessage, loading: false});
                    });


            // console.log('USERS DETAILS:', JSON.stringify(result));//---->>PUSH USER DETAILS TO DATA BASE
            // console.log('USERS DETAILS:',this.props.findStore.userDetails);//---->>PUSH USER DETAILS TO DATA BASE
        }
    };


    facebookLogin() {

        const {session}  =  this.state;
        if (!session.user)
            return LoginManager
                .logInWithReadPermissions(['public_profile', 'email'])
                .then((result) => {
                    if (!result.isCancelled) {
                        console.log(`Login success with permissions: ${result.grantedPermissions.toString()}`);
                        // get the access token
                        return AccessToken.getCurrentAccessToken().then((data) => {
                            const infoRequest = new GraphRequest(
                                '/me?fields=name,picture,email',
                                null,
                                this.responseInfoCallback
                            );
                            // Start the graph request.
                            console.log('base data:', data);
                            new GraphRequestManager().addRequest(infoRequest).start();
                        })
                    }
                })
                .then(data => {
                    if (data) {
                        console.log('new firebase credential ', data);
                        const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken);
                        return firebase.auth().signInWithCredential(credential)
                    }
                })
                .then((currentUser) => {
                    if (currentUser) {
                        console.info('currentUser', JSON.stringify(currentUser.toJSON()))
                    }

                })
                .catch((error) => {
                    console.log(`Login fail with error: ${error}`)
                })
        console.log('State', session.user)
        console.log('faceBookUser', this.props.store.faceBookUser)
        console.log('firebaseUser', this.props.store.firebaseUser)
    }

    async setUserDetails(userDetails) {
        await this.props.store.setUserDetails(userDetails);
        await this.props.store.getUserLocation();
        this.setState({
            session: {user: true}
        })
    }

    gotoMain(){
        Actions.Main()
    }
    render() {
        let image = this.state.session.user ? require('../Images/bluegif.gif') : require('../Images/bluegif.gif')
        return (
            <HomeComponent
                ActivityIndicatorLoadingView={this.ActivityIndicatorLoadingView}
                session={this.state.session.user}
                facebookLogin={this.facebookLogin.bind(this)}
                gotoMain={this.gotoMain.bind(this)}/>


        );
    }
}
export default App


// <MotionMenu
//     type="circle"
//     margin={120}
//     y={0}
//     bumpy
//     x={0}
//     openSpeed={60}
//     wing={false}
//     reverse={false}
//     onOpen={() => console.log('onOpen')}
//     onClose={() => console.log('onClose')}
// >
//     <TouchableOpacity className="button"> <View title="bars" className="View"><Icon  name="moon-o" size={20} color="#2eb5ee"/></View></TouchableOpacity>
// </MotionMenu>