/**
 * Created by jlmconsulting on 5/6/18.
 */
import React, {Component} from 'react';
import {StyleSheet, Text, TouchableWithoutFeedback, Animated, View} from 'react-native';


import {observer, inject, when} from 'mobx-react'

@inject('store')
@observer
class FlipCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ViewStyle: styles.flipCard,
            fontSize : 14
        };
    }

    componentWillMount() {
        this.animatedValue = new Animated.Value(0);
        this.value = 0;
        this.animatedValue.addListener(({value}) => {
            this.value = value;
        });

        this.frontInterpolate = this.animatedValue.interpolate({
            inputRange: [0, 180],
            outputRange: ['0deg', '180deg']
        });

        this.backInterpolate = this.animatedValue.interpolate({
            inputRange: [0, 180],
            outputRange: ['180deg', '360deg']
        });
    }

    flipCard(){
        if (this.value >= 90) {
            Animated.spring(this.animatedValue, {
                toValue: 0,
                friction: 8,
                tension: 10
            }).start();
        } else {
            Animated.spring(this.animatedValue, {
                toValue: 180,
                friction: 8,
                tension: 10
            }).start((finished)=> {
                if (finished)
                    this.props.store.iconComms(7)
            });
        }
    };

    render() {
        const {
            flipCardMessage = 'Sorry! \n Please try again?',
            flipCardColor,
           } = this.props.store;

        const frontAnimatedStyle = {
            transform: [{rotateY: this.frontInterpolate}]
        };
        const backAnimatedStyle = {
            transform: [{rotateY: this.backInterpolate}]
        };

        const {ViewStyle, fontSize} = this.state
        return (
            <TouchableWithoutFeedback onPress={this.flipCard.bind(this)}>
                <View>
                    <Animated.View style={[ViewStyle, frontAnimatedStyle, { backgroundColor: flipCardColor }]}>
                        <Text style={[styles.text, { fontSize }]}>{flipCardMessage.charAt(0).toUpperCase() + flipCardMessage.slice(1)}</Text>
                    </Animated.View>
                </View>
            </TouchableWithoutFeedback>
            
        );
    }
}

const styles = StyleSheet.create({
    flipCard: {
        width: 150,
        height: 150,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        backfaceVisibility: 'hidden',
        shadowOpacity: 0.4,
        shadowRadius: 10,
        shadowOffset: {width: 4, height: 4},
        shadowColor: '#000',
        elevation: 2,
        opacity: 0.9
    },
    hidden: {
        width: 0,
        height: 0,
    },
    flipCardBack: {
        position: 'absolute',
        top: 0
    },
    text: {
        color: '#ffffff',
        textAlign: 'center',
        paddingHorizontal: 10,
        paddingVertical: 10,
        fontWeight: 'bold'
    }
});

export default FlipCard;