import firebase from 'react-native-firebase'

const firebaseConfig = {
    apiKey: "AAAAdw92v0o:APA91bF-yF4s2azjAYbZfhH8PpJyOtmjBxWXMgLdBz_OeEDxjAVvpylg7U8-h5IoHyPLUjw4MzqbC-7klsTiKFtCNuWeRvJoAuiUWISwmxaKBPYDa_NdqCqQaSu_-WBplQXr1MyKRWvx",
    authDomain: "krops-a9384.firebaseapp.com",
    databaseURL: "https://krops-a9384.firebaseio.com/",
    projectId: "krops-a9384",
    storageBucket: "gs://krops-a9384.appspot.com",
    messagingSenderId: "511360548682"
};

let instance = null;

class FirebaseService {
  constructor() {
    if (!instance) {
      this.app = firebase.initializeApp(firebaseConfig);
      instance = this;
    }
    return instance
  }
}

const firebaseService = new FirebaseService().app;
export default firebaseService;
