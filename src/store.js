/**
 * Created by jlmconsulting on 5/2/18.
 */
import {observable, when, action, toJS} from 'mobx';
import {Alert} from 'react-native';
import {persist} from 'mobx-persist';
import {GoogleAnalyticsTracker} from 'react-native-google-analytics-bridge'
import RNPermissions from 'react-native-permissions';
import {regionFrom, getLatLonDiffInMeters} from './utils/helpers';
import {Actions} from 'react-native-router-flux';
import firebase from 'react-native-firebase'
import axios from 'axios'
import apiEncode  from './utils/apiEncode'
import Geocoder from 'react-native-geocoding';
import GalleryStore from './GalleryStore';
import _ from 'lodash';


Geocoder.setApiKey('AIzaSyABwo72uGqmjYtA0pH-mdJ4bIdd54g2kqU');
const ERR_MSG = 'emmmm...ERROR...';

class Store {

    @persist @observable coreGoal = [];
    @observable tracker = new GoogleAnalyticsTracker('UA-116900112-1');
    @observable dataLoading = false;
    @observable returnItem = {};
    @observable position = {};
    @observable coreText = {};

    @observable displayMenu = true;
    @observable shortCutItems = {
        "power-off": '#39ff94',
        "file-image-o": '#3941ff',
        "newspaper-o": 'black',
        "times-circle": '#ff9439',
        "check-circle": '#a4ff39',
        "question-circle": 'black',
        "info-circle": 'white'
    };
    @observable displayInput = false;
    @observable inPutMessage = 'Your Question?';
    @observable displayNote = true;
    
    @observable flipCardColor = "#9439FF";
    @observable flipCardMessage = 'Welcome! This is the RN Resume of Jason Mullings';

    @persist @observable searchData = {};
    @persist @observable userLocation = {};
    @persist @observable PhonePermission = {};
    @persist @observable faceBookUser = null;
    @persist @observable firebaseUser = null;
    @persist @observable token = {};
    @persist @observable storeState = {
        init: true,
        errMsg: null,
        forgotPass: false,
        email: '',
        password: '',
        session: {
            user: false,
            kf_user: false,
            kr_user: false,
            kl_user: false,
            ks_user: false,
            ki_user: false
        }
    };
    
    async iconComms(index, text) {
        console.log('index', index, text);
        try {
            switch (index) {
                case "info-circle":
                    return await this.setLightBox();
                    break;
                case "question-circle":
                    return await this.setInputField();
                    break;
                case "check-circle":
                    return await this.setCorrect();
                    break;
                case "times-circle":
                    return await this.setInCorrect();
                    break;
                case "newspaper-o":
                    return await this.setDocument();
                    break;
                case "file-image-o":
                    return await this.setImages();
                    break;
                case "power-off":
                    return await this.logOut();
                    break;
                case 7:
                    this.displayNote = false;
                    break;
                default:
                    return await this.setCoreReturn(index);
                    break;
            }

        } catch (e) {
            console.log('Location Error')
        }
    }

    async baseItems() {
        this.displayInput = !this.displayInput;
        this.displayNote = !this.displayNote;
        try {
            if (this.displayInput || this.displayNote)
                return await {
                    items: {
                        "power-off": '#39ff94',
                        "times-circle": '#ff9439',
                        "check-circle": '#a4ff39',
                        ...this.returnItem
                    },
                    showItems: true
                };
            return await this.setHomeMenu()
        } catch (e) {
            console.log(e)
        }
    }

    async setHomeMenu() {
        this.displayInput = false;
        this.displayNote = false;
        try {
            return await {
                items: {
                    "power-off": '#39ff94',
                    "times-circle": '#ff9439',
                    "check-circle": '#a4ff39',
                    "question-circle": 'black',
                    "info-circle": 'white'
                },
                showItems: false
            }
        } catch (e) {
            console.log(e)
        }
    }


    async setCoreText(key, value) {
        var obj = {};
        try {
            this.coreText = obj[key] = value;
            console.log('this.coreText', this.coreText)
        } catch (e) {
            console.log(e)
        }
    }

    async getStoreState() {
        try {
            return await toJS(this.storeState)
        } catch (e) {
            console.log(e)
        }
    }

    async setSessionUser(state) {
        try {
            this.storeState.session.user = state
        } catch (e) {
            console.log(e)
        }
    }

    async setUserDetails(state) {
        console.log("setUserDetails!!: ", state);
        try {
            this.faceBookUser = state
        } catch (e) {
            console.log(e)
        }
    }

    async getFireToken() {

        try {
            await firebase
                .messaging()
                .getToken()
                .then(token => {
                    console.warn("Device firebase Token: ", token);
                    this.token = token;
                });


        } catch (e) {
            console.log('ERROR', e);
        }
    }

    async logOut() {
        Actions.pop();
        return {
            items: {
                "power-off": '#39ff94',
                "file-image-o": '#3941ff',
                "newspaper-o": 'black',
                "times-circle": '#ff9439',
                "check-circle": '#a4ff39',
                "question-circle": 'black',
                "info-circle": 'white'
            }
        };

        try {
            await firebase
                .auth()
                .signOut()
                .then(() => {
                        firebase.auth().onAuthStateChanged(user => {
                            if (user) {
                                firebase.auth().signOut();
                            }
                        });
                    },
                    (error)=> {
                        // An error happened.
                        console.log('ERROR', error);
                    }
                );

        } catch (e) {
            console.log('ERROR', e);
        }
    }

    async getPermission(type) {
        try {
            var obj = {};
            return await RNPermissions.request(type)
                .then((response) => {
                    this.PhonePermission = obj[type] = response;
                })
                .catch((error) => {
                    console.log(error);
                });

        } catch (e) {
            console.log(ERR_MSG, e)
        }
    }

    async getCurrentLocation() {
        try {
            await navigator.geolocation.getCurrentPosition(
                (position) => {
                    this.userLocation = regionFrom(
                        position.coords.latitude,
                        position.coords.longitude,
                        position.coords.accuracy
                    );
                },
                // (error) => this.setState({error: error.message}),
                {enableHighAccuracy: false, timeout: 10000, maximumAge: 3000},
            );
            return  await this.userLocation
        } catch (e) {
            console.log('Location Error')
        }
    }


    ////LIGHTBOX
    async setLightBox() {

        try {
            // Actions.lightbox({imgSource: {uri: 'https://www.appcoda.com/wp-content/uploads/2015/04/react-native-1024x631.png'}});
            // this.flipCardMessage = 'Would you like to know a little more..?';
            this.displayInput = true;

            return await this.baseItems()
        } catch (e) {
            console.log('ERROR', e);
        }
    }

    async setInputField() {

        try {
            this.displayNote = true;
            return await this.baseItems()
        } catch (e) {
            console.log('ERROR', e);
        }
    }

    async setCorrect() {

        try {
            if (this.displayInput) {
                await this.getPostData(this.coreText, null);
                return await this.setLightBox()
            }
            if (this.displayNote) {
                this.getGetData(this.searchData, true)
            }
            return await this.setHomeMenu()
        } catch (e) {
            console.log('ERROR', e);
        }
    }

    async setInCorrect() {

        try {

            if (this.displayNote) {
                this.getGetData(this.searchData, false)
            }
            return await this.setHomeMenu()
        } catch (e) {
            console.log('ERROR', e);
        }
    }

    async setCoreReturn(index) {
        this.searchData.Icon.title = index;
        this.displayNote = false;
        let baseImg = 'https://www.appcoda.com/wp-content/uploads/2015/04/react-native-1024x631.png'
        let Icon = _.find(this.searchData.Icon,(res)=>res.icon==('fa-' + index))

        console.log(' let Icon',Icon.func, toJS(Icon))
        try {
            switch (Icon.func) {
                case "Chat":
                    Actions.Chat({data:this.searchData})
                    break;
                case "Directions":
                    Actions.Directions({data:this.searchData})
                    break;
                case "Gallery":
                    Actions.Gallery({data:this.searchData})
                    break;
                case "ImageUpload":
                    Actions.ImageUpload({data:this.searchData})
                    break;
                case "lightbox":
                    Actions.Slider({slideImage: {uri: Icon.image.secure_url || baseImg}});
                    break;
                case "Slider":
                    Actions.Slider({slideImage: {uri: Icon.image.secure_url || baseImg}});
                    break;
                case "Map":
                    Actions.Map({data:this.searchData})
                    break;
                case "Profile":
                    Actions.Profile({data:this.searchData})
                    break;
                case "VideoUpload":
                    Actions.VideoUpload({data:this.searchData})
                    break;
                case "WebView":
                    Actions.WebView({data:this.searchData})
                    break;
                default:
                    return Actions.Slider({slideImage: {uri: Icon.image.secure_url || baseImg}});
                    break;
            }

        } catch (e) {
            console.log(e)
        }
    }

    async setDocument() {

        try {
            // Actions.lightbox({imgSource: {uri: 'https://www.appcoda.com/wp-content/uploads/2015/04/react-native-1024x631.png'}});
            return await this.baseItems()
        } catch (e) {
            console.log('ERROR', e);
        }
    }

    async setImages() {

        try {
            // Actions.lightbox({imgSource: {uri: 'https://www.appcoda.com/wp-content/uploads/2015/04/react-native-1024x631.png'}});
            return await this.baseItems()
        } catch (e) {
            console.log('ERROR', e);
        }
    }

    async getGetData(text, res) {
        // console.log('getGetData',text)
        if (text)
            try {
                let bodyParameters = apiEncode({
                    person: this.faceBookUser || ['Jason Mullings', "manila", +new Date()],
                    ...toJS(text),
                    response: res
                });

                console.log('getGetData: ', bodyParameters);
                // 'http://192.168.1.4:3000/api/sentence?'
                await axios.get('https://til-crm.herokuapp.com/api/sentence?' + bodyParameters)
                    .then((response)=> {
                        if (response.data.Sentence !== null) {
                            console.log('response.data', response.data);
                            this.dataLoading = false;
                            this.searchData = response.data.Sentence;
                            this.coreGoal = this.searchData.Goal;
                            this.flipCardMessage = this.searchData.title;
                            this.flipCardColor = this.searchData.State || '#9439FF';
                        }
                        // let Icons = this.returnItem = this.searchData.Icon;
                        // let obj = {};
                        // Icons.map((res)=>{
                        //     return obj[res.icon.slice(3)] = '#'+Math.floor(Math.random()*16777215).toString(16);
                        // });
                        // console.log('response.data', response);
                        // console.log('Icons', Icons);
                    });
            } catch (e) {
                console.log('############### catch ###############');
                console.log('this.response', JSON.stringify(e));
                console.log('############### catch ###############')
            }
    }

    async getPostData(text, res) {

        if (text.length > 0)
            try {
                this.dataLoading = true;
                let bodyParameters = apiEncode({
                    title: text,
                    Icon: this.searchData.Icon,
                    person: this.faceBookUser ? [this.faceBookUser.name, "manila", +new Date()] : ['Jason Mullings', "manila", +new Date()],
                    response: res
                });

                console.log('getPostData: ', bodyParameters);
                // 'https://til-crm.herokuapp.com/api/sentence?'
                await axios.post('https://til-crm.herokuapp.com/api/sentence?' + bodyParameters)
                    .then((response)=> {
                        if (response) {
                            console.log('response.data', response.data.Sentence);
                            this.dataLoading = false;
                            this.searchData = response.data.Sentence;
                            let Icons = this.searchData.Icon;
                            let obj = {};
                            if (Icons.length > 0)
                                Icons.map((res)=> {
                                    if (res.like > 0)
                                        return obj[res.icon.slice(3)] = '#' + Math.floor(Math.random() * 16777215).toString(16);
                                });
                            this.returnItem = obj;
                            this.coreGoal = this.searchData.Goal;
                            this.flipCardColor = this.searchData.State || '#9439FF';
                            this.flipCardMessage = this.searchData.title;

                            return this.searchData;
                        }

                    });
            } catch (e) {
                this.dataLoading = false;
                console.log(ERR_MSG, e)
            }
    }

    async getUserLocation() {

        try {
            navigator.geolocation.getCurrentPosition((position) => {
                    var region = regionFrom(
                        position.coords.latitude,
                        position.coords.longitude,
                        position.coords.accuracy
                    );
                    this.position = {
                        coords: {
                            latitude: position.coords.latitude,
                            longitude: position.coords.longitude,
                            region: region
                        }
                    };
                    console.log('User Location set!!!!', toJS(this.position))
                }
            );
        } catch (e) {
            console.log(ERR_MSG, e)
        }

    }
}
const galleryStore = new GalleryStore();
const store = new Store();

export default  {store, galleryStore}