/**
 * Created by jlmconsulting on 5/2/18.
 */
import React, {Component} from 'react';
import {
    Platform,
    WebView,
    View,
    ActivityIndicator,
    Dimensions,
    ImageBackground,
    TouchableOpacity,
    Alert,
    Text
} from 'react-native';
import FlipCard from '../utils/flipCard'
import Makiko from '../utils/textInput'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Spinner from 'react-native-loading-spinner-overlay';
import ShortcutMenu from '../ShortcutMenu'
import styles from './Styles'

const HomeComponent = props => {

    let image = require('../Images/mainbg.jpg')
    return (

        <View style={styles.container}>
            <ImageBackground source={image} style={styles.ImageBackground}>

                    {props.displayInput ?
                        <View style={[ {padding: 12, width:250}, { backgroundColor: '#B9D4BB' }]}>
                            <Makiko
                                style={{ color: '#9439FF' }}
                                label={props.inPutMessage}
                                iconClass={FontAwesome}
                                iconName={'comment'}
                                iconColor={'white'}
                                inputStyle={{ color: '#9439FF' }}
                                onChangeText={(text) => props.updateState('question', text)}
                            />
                        </View>
                        : null}
                    {props.displayNote ?
                        <FlipCard
                            text={props.flipCardMessage}
                            backText=""
                            color="#9439FF"
                            backColor="#B9D4BB"
                            fontSize={20}
                        />
                        : null}
                    {props.displayMenu ?
                        <ShortcutMenu shortCutItems={props.shortCutItems}/>
                        : null}
                    <Spinner visible={props.dataLoading} textContent={"Loading...."} textStyle={{color: '#5f9f0d'}}/>

            </ImageBackground>
        </View>
 
    );
}

export default HomeComponent;