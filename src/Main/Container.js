/**
 * Created by jlmconsulting on 5/2/18.
 */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    Text,
    Platform,
    StyleSheet,
    WebView,
    View,
    ActivityIndicator,
    Dimensions,
    ImageBackground,
    TouchableOpacity,
    Alert
} from 'react-native';
import styles from './Styles'
import HomeComponent from './Component';
import {Actions} from 'react-native-router-flux';

import {observer, inject, when} from 'mobx-react'

@inject('store')
@observer
class App extends Component {
    
    async _updateState(key, value) {
        await this.props.store.setCoreText(key, value);
    };
    render() {
        const {displayMenu,shortCutItems, displayInput, inPutMessage, displayNote, dataLoading } = this.props.store
        return (
            <HomeComponent
                dataLoading={dataLoading}
                updateState={this._updateState.bind(this)}
                displayMenu={displayMenu}
                shortCutItems={shortCutItems}
                displayInput={displayInput}
                inPutMessage={inPutMessage}
                displayNote={displayNote}
                >
            </HomeComponent>


        );
    }
}
export default App
