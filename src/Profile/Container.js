/**
 * Created by jlmconsulting on 1/26/18.
 */


import React, { Component } from 'react'
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    PixelRatio,
    TouchableOpacity,
    Image,
    Button
} from 'react-native';

// import ProfilePageComponent from './Component';
import ImagePicker from 'react-native-image-picker';
import { Card, Avatar, FormLabel, FormInput } from "react-native-elements";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import styles from './Styles';


import {observer, inject} from 'mobx-react'

@inject('store')
@observer
class ProfilePageContainer extends Component {

    state = {
            data: {
                accessToken: 'EAAEMLSjMqigBACMeeKxckRHP8DQToVDk0LDGlcq5AfrjR0GIZCq4D4hDZAZB3bNhrEo2w4OzdUW1USiauScCqdfs1yYkEuDcZC6GhDQFqWaZCw9dKckTIuGOonysMTZAzPtB4o92i006eRRC9sZCA5qqOiTmaPldK66ENz67ZAYZAzxp6XWVuhV14ZAtXz5VuagfGqiuSTeqCl6wZDZD',
                permissions: ['public_profile', 'email'],
                declinedPermissions: [],
                applicationID: '294863074273832',
                accessTokenSource: 'WEB_VIEW',
                userID: '10156039495485396',
                expirationTime: 1522573175159,
                lastRefreshTime: 1517397823183
            },
            user: true,
            faceBook: {
                "id": "10156039495485396",
                "email": "jswan1972@yahoo.co.uk",
                "picture": {
                    "data": {
                        "url": "https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/14642017_10154541308235396_4436092501731787110_n.jpg?oh=3956df900d83f60cec84a1b1f6861c5c&oe=5AE0B836",
                        "is_silhouette": false,
                        "width": 50,
                        "height": 50
                    }
                },
                "name": "Jason Mullings"
            }

        }

    componentWillMount() {

        console.log('ProfileLog:',this.props)
        this.setPicture()
    }
    selectPhotoTapped() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    avatarSource: source
                });
            }
        });
        // <Image source={this.state.avatarSource} style={styles.uploadAvatar} />
    }

    selectVideoTapped() {
        const options = {
            title: 'Video Picker',
            takePhotoButtonTitle: 'Take Video...',
            mediaType: 'video',
            videoQuality: 'medium'
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled video picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                this.setState({
                    videoSource: response.uri
                });
            }
        });
    }

    setPicture(){

        console.log('this.state.faceBook.picture.data.url',this.state.faceBook.picture.data.url)
        if(this.state.faceBook.picture.data.url !== typeof 'undefined')
            this.setState({
                avatarSource: this.state.faceBook.picture.data.url
            })
        else
            this.setState({
                avatarSource: false
            })

    }
    render() {
        return (

            <KeyboardAwareScrollView behavior='padding'>

                <Card>
                    <View style={styles.container}>
                        <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>
                            <View style={[styles.avatar, styles.avatarContainer, {marginBottom: 20}]}>


                                { this.state.avatarSource === false ?   <Avatar
                                    xlarge
                                    rounded
                                    source={require('../Images/quick_add.png')}
                                    onPress={() => console.log("Works!")}
                                    activeOpacity={0.7}
                                /> :
                                    <Image style={styles.avatar} placeholder={require('../Images/quick_add.png')} source={require('../Images/quick_add.png')}/>
                                }
                            </View>
                        </TouchableOpacity>


                        { this.state.avatarSource &&
                        <Text style={{margin: 8, textAlign: 'center'}}>{this.state.avatarSource}</Text>
                        }

                    </View>


                    <FormLabel>Profile Id: { this.state.faceBook.id }</FormLabel>
                                     <FormLabel>Name</FormLabel>
                    <FormInput
                        defaultValue={ this.state.faceBook.name }
                    />
                    <FormLabel>Email</FormLabel>
                    <FormInput
                        defaultValue={ this.state.faceBook.email }
                    />
                    <FormLabel>Mobile</FormLabel>
                    <FormInput
                        defaultValue={ null }
                    />
                    <FormLabel>Address</FormLabel>
                    <FormInput />
                    <FormLabel>Address Line1</FormLabel>
                    <FormInput />
                    <FormLabel>Address Line2</FormLabel>
                    <FormInput />
                    <FormLabel>Area</FormLabel>
                    <FormInput />
                    <FormLabel>City</FormLabel>
                    <FormInput />
                    <FormLabel>Country</FormLabel>
                    <FormInput />

                   <Text style={{ color:'black' }}>Save</Text>
                </Card>
            </KeyboardAwareScrollView>
        );
    }
    launchCamera(){
        // Launch Camera:
        ImagePicker.launchCamera(options, (response)  => {
            // Same code as in above section!
        });

// Open Image Library:
        ImagePicker.launchImageLibrary(options, (response)  => {
            // Same code as in above section!
        });
    }
}


export default ProfilePageContainer
