/**
 * Created by jlmconsulting on 3/24/18.
 */
import React, {Component} from 'react';
import {
    View,
    Alert,
    TouchableOpacity,
    Text,
    Image
} from 'react-native';
import MapView from 'react-native-maps';
import styles from './StyleSheet'
//
const KDDriver = props => {

   console.log('this.state',props.state.picture)
    return (

        <View style={styles.container}>

            <MapView style={styles.map} initialRegion={{
                   latitude:14.6849688,
                   longitude:121.1054654,
                   latitudeDelta: 1,
                   longitudeDelta: 1
                  }}>
                {!!props.state.latitude && !!props.state.longitude &&
                <MapView.Marker.Animated
                    coordinate={{"latitude":props.state.latitude,"longitude":props.state.longitude}}
                    key={props.state.id}
                    title={props.state.id}
                    image={require('../Images/quick_add.png')}
                >
                </MapView.Marker.Animated>}
            </MapView>
            <TouchableOpacity style={styles.findButton} onPress={props.handleMarkerPress}>
                <Text style={styles.findButtonText}>Find me!</Text>
            </TouchableOpacity>
        </View>
    );
};

export default KDDriver
