import React, {Component} from 'react';
import {
    View,
    Alert
} from 'react-native';
import {Actions} from 'react-native-router-flux'
import Geocoder from 'react-native-geocoding';
import KFFinderComponent from './Component';
import {regionFrom, getLatLonDiffInMeters} from '../utils/helpers';
import {observer, inject} from 'mobx-react'

Geocoder.setApiKey('AIzaSyABwo72uGqmjYtA0pH-mdJ4bIdd54g2kqU');

@inject('store')
@observer
class KFFinder extends Component {
    state = {
        picture: null,
        region: {
            latitude: null,
            longitude: null
        }
    };

    componentWillMount() {

        this.getUserDetails();
    }

    async getUserDetails() {
        const userDetails = await this.props.store.getCurrentLocation()
        this.setState({
            ...userDetails
        })
        console.log('getUserDetails',this.state)
    }


    handleMarkerPress() {
        this.getUserDetails()
    }

    render() {

        return (
            <KFFinderComponent
                state={this.state}
                handleMarkerPress={this.handleMarkerPress.bind(this)}
            />
        );
    }

    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.watchId);
    }
}

export default  KFFinder
