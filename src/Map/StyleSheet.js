/**
 * Created by jlmconsulting on 3/24/18.
 */

import React, {Component} from 'react';
import {StyleSheet, View, Button, Alert, Dimensions, Platform} from 'react-native';

const deviceDimes = Dimensions.get('window');
const paddedWidth = deviceDimes.width-30;

export default StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    map: {
        ...StyleSheet.absoluteFillObject
    },
    findButton:{
        backgroundColor:'#5f9f0d',
        padding:15,
        alignItems:'center',
        justifyContent:'center',
        width:deviceDimes.width-60,
        position:'absolute',
        zIndex:2,
        bottom:30,
        left:30,
        borderRadius:5
    },
    findButtonText:{
        color:'#fff',
        // ...Platform.select({
        //     android: {fontFamily: 'GothamBook'},
        //     ios: {fontFamily: 'Gotham-Light'}
        // }),
        fontSize:16
    },deliveryCardTop:{
        backgroundColor:'#fff',
        flexDirection:'row',
        alignItems:'center',
        width: paddedWidth
    },
    riderImage:{
        height:75,
        width:75,
        borderRadius:50,
        marginRight:10
    },
    riderDetails:{
        flex:1
    },
    detailsRow:{
        flexDirection:'row'
    },
    detailsContainer:{
        flex:1
    },
    detailsName:{
      // ...Platform.select({
      //     android: {fontFamily: 'GothamBold'},
      //     ios: {fontFamily: 'Gotham-Black'}
      // }),
        fontSize:16,
        color:"#000",
        marginBottom:15
    },
    detailsOptions:{
        color:'#5f9f0d'
    },
    detailsText:{
        color:'#999',
        // ...Platform.select({
        //     android: {fontFamily: 'GothamBook'},
        //     ios: {fontFamily: 'Gotham-Light'}
        // }),
        fontSize:12,
        marginBottom:15
    },
    detailsTime:{
        color:'#999',
        // ...Platform.select({
        //     android: {fontFamily: 'GothamBook'},
        //     ios: {fontFamily: 'Gotham-Light'}
        // }),
        fontSize:10
    }
});
