/**
 * Created by jlmconsulting on 1/26/18.
 */
import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View
} from 'react-native';
import styles from './Styles'
import styles from './Styles';

const LoginComponent = props => {
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>
                <View style={[styles.avatar, styles.avatarContainer, {marginBottom: 20}]}>
                    { props.avatarSource === null ? <Text>Select a Photo</Text> :
                        <Image style={styles.avatar} source={props.avatarSource}/>
                    }
                </View>
            </TouchableOpacity>
            { props.videoSource &&
            <Text style={{margin: 8, textAlign: 'center'}}>{props.videoSource}</Text>
            }
        </View>
    )
}


export default LoginComponent


