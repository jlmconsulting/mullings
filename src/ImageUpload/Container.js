/**
 * Created by jlmconsulting on 1/26/18.
 */


import React, {Component} from 'react'
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    PixelRatio,
    TouchableOpacity,
    Image,
    Button
} from 'react-native';

// import ImageUploadComponent from './Component';
import ImagePicker from 'react-native-image-picker';
import {Card, Avatar, FormLabel, FormInput} from "react-native-elements";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import styles from './Styles';

class ImageUploadContainer extends Component {
    state = {
        avatarSource: null,
        videoSource: null
    };

    selectPhotoTapped() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = {uri: response.uri};

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    ...response
                });
            }
        });
        // <Image source={this.state.avatarSource} style={styles.uploadAvatar} />
    }

    handleSaveImage(){
        // var data = new FormData()
        // data.append('haha', 'input')

        fetch('https://til-crm.herokuapp.com/cognitive', {
            method: 'post',
            body: this.state.avatarSource
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.log('Fetch Success==================');
                console.log(responseData);

            })
            .catch((error) => {
                console.warn(error);
            })
            .done();
    }
    render() {

        console.log('this.state.avatarSource',this.state.avatarSource)
        return (
            <KeyboardAwareScrollView behavior='padding'>
                {this.state.avatarSource === null ?
                    <View style={styles.container}>
                        <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>
                            <View style={[styles.avatar, styles.avatarContainer, {marginBottom: 20}]}>
                                 <Text>Select a Photo</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    :
                    <Card>
                        <View style={styles.container}>
                            <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>
                                <View style={[styles.avatar, styles.avatarContainer, {marginBottom: 20}]}>
                                    <Image style={styles.avatar} source={this.state.avatarSource.path}/>

                                </View>
                            </TouchableOpacity>
                            { !!this.state.avatarSource &&
                            <Text style={{margin: 8, textAlign: 'center'}}>{this.state.avatarSource}</Text>
                            }

                        </View>
                        <FormLabel>Title</FormLabel>
                        <FormInput />
                        <FormLabel>Description</FormLabel>
                        <FormInput />

                        <Button block primary onPress={this.handleSaveImage.bind(this)}><Text style={{ margin:20, color:'brown' }}>Save</Text></Button>
                    </Card>
                }
            </KeyboardAwareScrollView>
        );
    }

    launchCamera() {
        // Launch Camera:
        ImagePicker.launchCamera(options, (response) => {
            // Same code as in above section!
        });

// Open Image Library:
        ImagePicker.launchImageLibrary(options, (response) => {
            // Same code as in above section!
        });
    }
}

export default ImageUploadContainer
