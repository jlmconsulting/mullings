/**
 * Created by jlmconsulting on 1/26/18.
 */


import React, {Component} from 'react'
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    PixelRatio,
    TouchableOpacity,
    Image,
    Button
} from 'react-native';

// import VideoUploadComponent from './Component';
import ImagePicker from 'react-native-image-picker';
import {Card, Avatar, FormLabel, FormInput} from "react-native-elements";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import styles from './Styles';

class VideoUploadContainer extends Component {
    state = {
        avatarSource: null,
        videoSource: null
    };

    selectVideoTapped() {
        const options = {
            title: 'Video Picker',
            takePhotoButtonTitle: 'Take Video...',
            mediaType: 'video',
            videoQuality: 'medium'
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled video picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                this.setState({
                    videoSource: response.uri
                });
            }
        });
    }

    render() {
        return (
            <KeyboardAwareScrollView behavior='padding'>
                {this.state.videoSource === null ?
                    <View style={styles.container}>
                        <TouchableOpacity onPress={this.selectVideoTapped.bind(this)}>
                            <View style={[styles.avatar, styles.avatarContainer]}>
                                <Text>Select a Video</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    :
                    <Card>
                        <View style={styles.container}>
                            <TouchableOpacity onPress={this.selectVideoTapped.bind(this)}>
                                <View style={[styles.avatar, styles.avatarContainer]}>
                                    <Image style={styles.avatar} source={this.state.videoSource}/>
                                </View>
                            </TouchableOpacity>

                            { this.state.videoSource &&
                            <Text style={{margin: 8, textAlign: 'center'}}>{this.state.videoSource}</Text>
                            }
                        </View>
                        <FormLabel>Title</FormLabel>
                        <FormInput />
                        <FormLabel>Description</FormLabel>
                        <FormInput />

                        <Text style={{ margin:20, color:'brown' }}>Save</Text>
                    </Card>
                }
            </KeyboardAwareScrollView>
        );
    }

    launchCamera() {
        // Launch Camera:
        ImagePicker.launchCamera(options, (response) => {
            // Same code as in above section!
        });

// Open Image Library:
        ImagePicker.launchImageLibrary(options, (response) => {
            // Same code as in above section!
        });
    }
}

export default VideoUploadContainer
// <VideoUploadComponent >

// </VideoUploadComponent>