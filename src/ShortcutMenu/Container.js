/**
 * Created by jlmconsulting on 5/7/18.
 */
import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Animated,Dimensions } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {observer, inject} from 'mobx-react'
import styles from './Styles'


const height = Dimensions.get('window').height;
const getTransformStyle = animation => {
    return {transform: [{translateY: animation}]};
};

type Props = {
    color?: string,
    onPress?: Function
};
type State = {
    fabs: Array<any>,
    animate: any
};


@inject('store')
@observer
class ShortcutMenu extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            animate: new Animated.Value(0),
            fabs: [
                new Animated.Value(0),
                new Animated.Value(0),new Animated.Value(0),
                new Animated.Value(0),new Animated.Value(0),
                new Animated.Value(0), new Animated.Value(0)],
            items: {
                "power-off":'#39ff94',
                "file-image-o":'#3941ff',
                "newspaper-o":'black',
                "times-circle":'#ff9439',
                "check-circle":'#a4ff39',
                "question-circle":'black',
                "info-circle":'white'
            }}
        this.open = false;
        this.handlePress =this.handlePress.bind(this)
    }

    open: boolean;

    componentDidMount(){

        const that = this
        setTimeout( () =>{
            that.setBase()
        }, 3000);
        this.handlePress()
    }

    handlePress() {
        const toValue = this.open ? 0 : 1;

        const flyouts = this.state.fabs.map((value, i) => {
            return Animated.spring(value, {
                toValue: (i + 1) * -(height/8.3) * toValue,
                friction: 5,
                useNativeDriver: true
            });
        });

        Animated.parallel([
            Animated.timing(this.state.animate, {
                toValue,
                duration: 500,
                useNativeDriver: true
            }),
            Animated.stagger(10, flyouts)
        ]).start();

        this.open = !this.open;
    };

    async clickMoreBtn(index) {
        this.handlePress()
        const response = await this.props.store.iconComms(index)
        this.setState({
            ...response
        })
        if(this.state.showItems)
            this.handlePress()
    }
    async setBase() {
         const response = await this.props.store.setHomeMenu()
         this.setState({
             ...response
         })
         this.handlePress()
    }
    render() {
        const { color = '#9439FF', fabColor = '#dec3ffs' } = this.props;
        // const fabColorInterpolate = this.state.animate.interpolate({
        //   inputRange: [0, 1],
        //   outputRange: ['rgb(24,214,255)', 'cornflowerblue']
        // });

        const fabRotate = this.state.animate.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '135deg']
        });

        const fabStyle = {
            transform: [
                {
                    rotate: fabRotate
                }
            ]
        };
        const {items} = this.state
        const keys = Object.keys(items);
        const that = this
        return (
            <View style={[styles.position]}>
                {this.state.fabs.map((animation, i) => {
                    let size = 43
                    if(i==1||i==2)
                        size = 37
                    return (
                        <TouchableOpacity
                            key={i}
                            style={[
                                styles.button,
                                styles.fab,
                                { backgroundColor: items[keys[i]] },
                                getTransformStyle(animation)
                              ]}
                            onPress={() => that.clickMoreBtn(keys[i])}>
                            <FontAwesome name={keys[i]} size={size} style={styles.navIconOn}/>
                        </TouchableOpacity>
                    );
                })}

                <TouchableOpacity onPress={this.handlePress}>
                    <Animated.View style={[styles.button, fabStyle, { backgroundColor: color }]}>
                        <Text style={styles.plus}>+</Text>
                    </Animated.View>
                </TouchableOpacity>
            </View>
        );
    }
}
export default ShortcutMenu

