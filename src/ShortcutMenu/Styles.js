/**
 * Created by jlmconsulting on 5/2/18.
 */
/**
 * Created by jlmconsulting on 1/26/18.
 */
import {
    StyleSheet,
    Dimensions,
    Platform
} from 'react-native';

const deviceDimes = Dimensions.get('window');
const paddedWidth = deviceDimes.width-60;

export default StyleSheet.create({
    position: {
        position: 'absolute',
        right: 15,
        bottom: 15
    },
    fab: {
        position: 'absolute',
        bottom: 0,
        right: 0
    },
    button: {
        width: 48,
        height: 48,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    plus: {
        fontWeight: 'bold',
        fontSize: 25,
        color: 'white'
    }
});