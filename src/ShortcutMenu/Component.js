/**
 * Created by jlmconsulting on 5/2/18.
 */
import React, {Component} from 'react';
import {
    Platform,
    WebView,
    View,
    ActivityIndicator,
    Dimensions,
    ImageBackground,
    TouchableWithoutFeedback,
    Alert
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import styles from './Styles'
import ShortcutMenu from '../ShortcutMenu'

const ShortComponent = props =>{

    return (
        <View {...props._panResponder.panHandlers}
            style={{position:'absolute',bottom:bgBottom, flexDirection:'column',right:bgRight,height:bgHeight,width:bgWidth,}}>
            {this.moreBtnView.bind(this)(plusBottom,plusRight)}
            <TouchableWithoutFeedback onPress={props.changeValue} >
                <Animated.Image style={{position:'absolute',bottom:plusBottom,right:plusRight, width: plusButtonW,
                                        height: plusButtonW, alignSelf: 'center', transform:[{rotate: props.state.fadeAnim.interpolate({
                                            inputRange:[0,moreHeight],
                                            outputRange:['0deg','90deg']
                                        })}]}}
                                source={quickImage}/>
            </TouchableWithoutFeedback>
        </View>
    );
}

export default ShortComponent;