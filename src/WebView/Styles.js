/**
 * Created by jlmconsulting on 5/2/18.
 */
/**
 * Created by jlmconsulting on 1/26/18.
 */
import {
    StyleSheet,
    Dimensions,
    Platform
} from 'react-native';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default StyleSheet.create({
    webview: {
        flex: 1,
        width: deviceWidth,
        height: deviceHeight
    }
});