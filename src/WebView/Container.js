import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Dimensions
} from 'react-native';
import { WebView } from 'react-native';
import styles from './Styles'
import {observer, inject, when} from 'mobx-react'

@inject('store')
@observer
class App extends Component{

    componentWillMount(){

        this.setState({
            SearchURI:"https://duckduckgo.com/?q=AI&t=ffhp&atb=v100-3&ia=web",
            ...this.props
        })

        console.log('SearchURI',this.state)
    }

    render() {
        const { data } = this.props;
        // const { searchURI } = this.props.store
        return (
            <View style={{flex:1}}>
                <WebView
                    style={styles.webview}
                    source={{uri:data.uri||this.state.SearchURI}}
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    startInLoadingState={false}
                    scalesPageToFit={true} />
            </View>
        );
    }
}
export default App
